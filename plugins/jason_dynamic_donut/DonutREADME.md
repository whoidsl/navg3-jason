
**JASON DYNAMIC DONUT**

                  ,,ggddY""""Ybbgg,,
                 ,agd""'              `""bg,
              ,gdP"                       "Ybg,
            ,dP"                             "Yb,
          ,dP"         _,,ddP"""Ybb,,_         "Yb,
         ,8"         ,dP"'         `"Yb,         "8,
        ,8'        ,d"                 "b,        `8,
       ,8'        d"                     "b        `8,
       d'        d'                       `b        `b
       8         8                         8         8
       8   (Y)   8           (X)           8         8       (Z)
       8         8                         8         8
       8         Y,                       ,P         8
       Y,         Ya                     aP         ,P
       `8,         "Ya                 aP"         ,8'
        `8,          "Yb,_         _,dP"          ,8'
         `8a           `""YbbgggddP""'           a8'
          `Yba                                 adP'
            "Yba             SAFE            adY"
              `"Yba,                     ,adP"'
                 `"Y8ba,             ,ad8P"'
                      ``""YYbaaadPP""''
**(X)** = Beacon
**(Y)** = JASON
**(Z)** = Ship
**SAFE:** Area between inner and outer circle are safe operating zone for Jason


The purpose of the Jason ROV Dynamic Donut is to demonstrate the spacing between the cable and the vehicle. 


**Motivation:**
	In ROV operations, the tether/cable is incredibly important, since it provides power, communications, and a backup recovery method for the vehicle. It’s incredibly heavy, especially when the vehicle is thousands of meters deep. It’s quite strong, since it’s armored. Operators must pay close attention to make sure that the cable is not put under high tension, both to make sure that the cable doesn’t break, and to make sure that the vehicle is not pulled unexpectedly during delicate operations.

Since the ship is bobbing around in surface waves, wind, and current, the ship motion needs to be decoupled from the rov(JASON). Spooling out more cable gives more freedom of movement, but also poses a hazard for the vehicle to get tangled. Tightening the cable constricts the vehicle motion, but also weighs less heavily and is an easier hazard to avoid.
	
The solution that Jason uses is to add orange syntactic foam “football floats” to the cable near the vehicle, then a beacon further up towards the ship. This creates a catenary curve that lifts the cable away from the ROV and its propellers. From the beacon onward, the cable climbs vertically through the water column. Based on the relative depths and XY positions of Jason and the beacon, there’s a safe operating area for the ROV that kind of looks like nested concentric cones in 3-space, or a donut in 2D.

There’s a couple ways that operators can adjust their geometry to keep Jason in the donut. Of course, you can move Jason fairly independently in/out of the donut. Pulling in on the winch moves the beacon UP in the water column, making the donut smaller, and letting out moves the beacon DOWN in the water column, making the donut wider. Moving the ship on the surface moves the XY position of the beacon, which is the center of the circle.

The radii of the inner and outer circle of the Dynamic donut are adjusted dynamically based on the delta depth between the rov and the beacon. A CSV file is used in NavG for some standards on how far to adjust the donut based on current delta depth. Once delta depth is taken into account, new donut radii values will be calculated using the range between the csv predefined depths, and current deltadepth between the rov and beacon(medea).

The donut is to be drawn on screen during Jason operation to provide visualization of safe operating range for Jason. 

**How was this tested?** 
Has not been tested at sea using real time data. Constructed based on DAT replay files of jason / ship / and beaconing position. Tested against navg2 for parity. Using a replay file, the calculations for the donut radii are equal at all time stamps on navg2 and navg3. If using a partially edited replay file to show more extremes of delta depth, donut in navg3 changes more repidly and dynamically, and sitll conforms to the same results as navg2.  Also tested using real jason cruise replay. Same donut radius results. 

**To Run**
Once project is compiled, Dynamic donut shared library will be created. This SO can be loaded in by using the plugin menu in navg. Select show plugin to be able to see the donut GUI in navg. A Donut layer will be created if VFR updates for Jason and Beacon are found. Also required: a donut csv with predefined limitations for deltadelpth and donut radii must be loaded in. If not loaded in donut will not be created or function properly.
