/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#include "DonutLayer.h"
#include <QGraphicsPathItem>
#include <QPainterPath>
#include <QPen>
#include <dslmap/Proj.h>

namespace navg {
namespace plugins {
namespace donut {

Q_LOGGING_CATEGORY(plugin_donutLayer, "navg.plugins.donut_layer")

class DonutLayerPrivate {

public:
  QPen m_pen;
  QGraphicsPathItem *m_donut_item = nullptr;
  QRectF m_bounding_rect;
  QMenu *m_menu = nullptr;
  QPointF m_donut_center;
};

DonutLayer::DonutLayer(QGraphicsItem *parent)
    : dslmap::MapLayer(parent), d_ptr(new DonutLayerPrivate) {

  Q_D(DonutLayer);
  setFlag(ItemHasNoContents, true);

  d->m_menu = MapLayer::contextMenu();
}
DonutLayer::DonutLayer(double inner_radius, double outer_radius, QPointF center,
                       QGraphicsItem *parent)
    : dslmap::MapLayer(parent), d_ptr(new DonutLayerPrivate) {

  Q_D(DonutLayer);
  setFlag(ItemHasNoContents, true);
  d->m_pen = QPen{Qt::red};
  d->m_pen.setWidth(4);

  d->m_menu = MapLayer::contextMenu();

  donut.inner_radius = inner_radius;
  donut.outer_radius = outer_radius;
  donut.center = center;
  update(UpdateDraw); // since we have the initial coordinates on creation we
                      // can draw , which also creates the bounding rect
}
DonutLayer::~DonutLayer() = default;

void DonutLayer::showPropertiesDialog() {
  auto widget = new DonutPropertiesWidget();
  widget->setLayer(this);
  widget->show();
}
std::unique_ptr<DonutLayer> DonutLayer::fromSettings(QSettings *settings) {

  auto layer = std::make_unique<DonutLayer>();
  layer->loadSettings(settings);
  return layer;
}
bool DonutLayer::saveSettings(QSettings &settings) {
  return saveSettings(&settings);
}
bool DonutLayer::loadSettings(QSettings &settings) {
  return loadSettings(&settings);
}

bool DonutLayer::saveSettings(QSettings *settings) {
  settings->setValue("name", name());
  Q_D(DonutLayer);

  savePenSettings(&d->m_pen, settings, "pens/donut/");

  // save donutcsv

  return true;
}

void DonutLayer::savePenSettings(QPen *pen_ptr, QSettings *settings,
                                 QString prefix) {
  settings->setValue(prefix + "width", pen_ptr->width());
  settings->setValue(prefix + "color", pen_ptr->color().name());
  settings->setValue(
      prefix + "style",
      QMetaEnum::fromType<Qt::PenStyle>().valueToKey(pen_ptr->style()));
}


bool DonutLayer::loadSettings(QSettings *settings) {
  setName(settings->value("name", "").toString());

  Q_D(DonutLayer);

  return true;
}

QRectF DonutLayer::boundingRect() const {
  const Q_D(DonutLayer);

  return d->m_bounding_rect;
}

void DonutLayer::paint(QPainter *, const QStyleOptionGraphicsItem *,
                       QWidget *) {}

bool DonutLayer::update(UpdateOptions options) {

  // qCDebug(plugin_donutLayer) << "Calling update with" << options;
  Q_D(DonutLayer);
  if (options == UpdateOption::UpdateDraw) {

    this->drawDonut();
  }
  return false;
}
void DonutLayer::receiveDonutValues(double inner_radius, double outer_radius,
                                    QPointF latLon) {

  donut.inner_radius = inner_radius;
  donut.outer_radius = outer_radius;
  donut.center = latLon;

  update(UpdateDraw);
}
void DonutLayer::drawDonut() {

  auto proj = projectionPtr();
  if (!proj) {
    return;
  }
  // switching because we want lonlat when drawing
  double y_start = donut.center.x();
  double x_start = donut.center.y();

  QPainterPath path;

  proj->transformFromLonLat(1, &x_start, &y_start);

  path.addEllipse(QPointF(x_start, y_start), donut.inner_radius, donut.inner_radius);
  path.addEllipse(QPointF(x_start, y_start), donut.outer_radius, donut.outer_radius);

  Q_D(DonutLayer);

  if (d->m_donut_item) {
    // donut item already exists so just set new path

    d->m_donut_item->setPath(path);
  } else {

    d->m_donut_item = new QGraphicsPathItem(path, this);
  }

  d->m_donut_item->setPen(d->m_pen);

  // prepareGeometryChange();
  d->m_bounding_rect = d->m_donut_item->boundingRect();
}

void DonutLayer::setProjection(std::shared_ptr<const dslmap::Proj> proj) {

  if (!proj) {
    qCDebug(plugin_donutLayer) << "no proj exists,setting default ";

    return;
  }

  auto p = projectionPtr();

  if (p && p->isSame(*proj)) {
    qCDebug(plugin_donutLayer)
        << "projectionptr exists and is same as passed in proj";
    return;
  }

  MapLayer::setProjection(proj);
}

void DonutLayer::setPenChange(const QPen &pen) {
  Q_D(DonutLayer);
  d->m_pen = pen;
  update(UpdateDraw);
}

QPen DonutLayer::getPen() {
  Q_D(DonutLayer);
  return d->m_pen;
}

void DonutLayer::testGeodeticConversion() {
#if 0
  QPointF point1(77.0, 38.0);
  QPointF point2(77, 38.0003);

  auto p = projectionPtr();
  const auto range_m = p->geodeticDistance(point1, point2);

  qCDebug(plugin_donutLayer) << "draw distance is " << range_m;

  QPointF localP(77.0, 38.0);
  QVector<QPointF> feee = {localP};
  auto c = projectionPtr();
  c->transformFromLonLat(feee, true);

  qCDebug(plugin_donutLayer) << "77,38 to local is" << feee[0];
  double x_val = 77.0;
  double y_val = 38.0;
  p->transformFromLonLat(1, &x_val, &y_val);

  qCDebug(plugin_donutLayer) << "transformed x from scene proj is " << x_val
                             << " and y is " << y_val;
#endif
}

} // donut
} // plugins
} // navg
