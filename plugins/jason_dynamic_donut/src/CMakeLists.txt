set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTORCC ON)

#find_package(navg)
#find_package(dslmap)
find_package(Qt5 5.5 REQUIRED COMPONENTS Network)

#
# The main jason_dynamic_donut library
#
add_library(navg_jason_dynamic_donut SHARED
  ${PROJECT_SOURCE_DIR}/include/jason_dynamic_donut/DynamicDonut.h
  DynamicDonut.cpp
  DynamicDonut.ui

  ${PROJECT_SOURCE_DIR}/include/jason_dynamic_donut/DonutLayer.h
  DonutLayer.cpp
  
  ${PROJECT_SOURCE_DIR}/include/jason_dynamic_donut/DonutPropertiesWidget.h
  DonutPropertiesWidget.cpp
  DonutPropertiesWidget.ui
  )

target_include_directories(navg_jason_dynamic_donut
  PUBLIC
  $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/include/jason_dynamic_donut>
  $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/include>
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/include/jason_dynamic_donut>
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/include>
  $<INSTALL_INTERFACE:include>
  PRIVATE
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
  $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
  )

set_target_properties(navg_jason_dynamic_donut
  PROPERTIES
  CXX_STANDARD 14
  CXX_STANDARD_REQUIRED ON
  )

target_compile_options(navg_jason_dynamic_donut
  PRIVATE -Werror -Wall
  )

target_link_libraries(navg_jason_dynamic_donut
  PUBLIC  navg::navg_library Qt5::Network
  )


#
# Install targets
#
include(GNUInstallDirs)
install(TARGETS navg_jason_dynamic_donut
  EXPORT navg_jason_dynamic_donutTargets
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
  )

#
# Install public headers
#
install(DIRECTORY ${PROJECT_SOURCE_DIR}/include/jason_dynamic_donut DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/navg/plugins)

#
# Create CMake export files for project
#
install(EXPORT navg_jason_dynamic_donutTargets
  FILE navg_jason_dynamic_donut-targets.cmake
  NAMESPACE navg::
  DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/navg_jason_dynamic_donut
  )

include(CMakePackageConfigHelpers)

#
# Create a CMake config file for external projects
#
configure_package_config_file(
  ${PROJECT_SOURCE_DIR}/cmake/navg_jason_dynamic_donut-config.cmake.in
  ${PROJECT_BINARY_DIR}/cmake/navg_jason_dynamic_donut-config.cmake
  INSTALL_DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/navg_jason_dynamic_donut
)

#
# Create a CMake version file.
#
write_basic_package_version_file(
  ${PROJECT_BINARY_DIR}/cmake/navg_jason_dynamic_donut-config-version.cmake
  VERSION ${navg_jason_dynamic_donut_VERSION}
  COMPATIBILITY AnyNewerVersion
)

install(
  FILES
  ${PROJECT_BINARY_DIR}/cmake/navg_jason_dynamic_donut-config.cmake
  ${PROJECT_BINARY_DIR}/cmake/navg_jason_dynamic_donut-config-version.cmake
  DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/navg_jason_dynamic_donut
)

