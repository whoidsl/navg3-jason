/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#include "DynamicDonut.h"
#include "navg/NavGMapScene.h"
#include "ui_DynamicDonut.h"
#include <QAction>
#include <QFileDialog>
#include <QMessageBox>
#include <QSettings>
#include <QTcpSocket>
#include <QTimer>
#include <dslmap/MapView.h>

namespace navg {
namespace plugins {
namespace donut {

Q_LOGGING_CATEGORY(plugin_donut, "navg.plugins.dynamic_donut")

DynamicDonut::DynamicDonut(QWidget *parent)
    : QWidget(parent), ui(std::make_unique<Ui::DynamicDonut>()),
      m_layer(nullptr), m_view(nullptr) {
  ui->setupUi(this);
  this->connectUI();
  rovDepth = UNITIALIZED_DEPTH;
  wireDepth = UNINITIALIZED_DEPTH;
  deltaDepth = UNINITIALIZED_DEPTH;
  fileOK = false;
  // default vehicle and beacon presets
  rov_vehicle_id = 0;
  wire_vehicle_id = 1;

  rov_fix = "SOLN_USBL";
  wire_fix = "SOLN_USBL";

  layer_pen = QPen{Qt::red};
  layer_pen.setWidth(4);

  donutTimer = new QTimer();
  donutTimer->setInterval(1000);
  connect(donutTimer, &QTimer::timeout, this, &DynamicDonut::donutTimeout);
  donutTimer->start();
}

DynamicDonut::~DynamicDonut() = default;

void DynamicDonut::setMapView(QPointer<dslmap::MapView> view) { m_view = view; }

void DynamicDonut::connectPlugin(const QObject *plugin,
                                 const QJsonObject &metadata) {
  if (!plugin) {
    return;
  }

  const auto name =
      metadata.value("MetaData").toObject().value("name").toString();
  if (name.isEmpty()) {
    return;
  }

  if (name == "navest") {
    if (!connect(plugin, SIGNAL(remoteAddressChanged(QHostAddress)), this,
                 SLOT(setNavestRemoteAddress(QHostAddress)))) {
      qCCritical(plugin_donut, "Unable to connect to signal: "
                               "Navest::remoteAddressChanged("
                               "QHostAddress)");
      Q_ASSERT(false);
    } else {
      qCDebug(plugin_donut, "connected to navest");
    }
    connect(plugin,
            SIGNAL(vfrMessageReceived(int, QString, double, double, double)),
            this, SLOT(handleVfrUpdate(int, QString, double, double, double)));
  }
}

void DynamicDonut::loadSettings(QSettings *settings) {
  if (settings == nullptr) {
    return;
  }
  fileOK = this->readDonutFromSettings(settings, "csv/");
  this->loadQPenLayerSettings(settings, "pens/donut/");
  this->loadDonutBeacons(settings, "beacons/");

  if (!m_layer) {
    m_layer = DonutLayer::fromSettings(settings).release();
    m_layer->loadSettings(settings);
    m_layer = nullptr;
  } else {
    m_layer->loadSettings(settings);
  }
}

void DynamicDonut::loadQPenLayerSettings(QSettings *settings, QString prefix) {
  bool ok = false;
  layer_pen.setWidth(settings->value(prefix + "width").toInt());

  auto value = settings->value(prefix + "color");
  if (value.isValid()) {
    const auto color = QColor{value.toString()};
    if (color.isValid()) {
      layer_pen.setColor(color);
    }
  }

  value = settings->value(prefix + "style");
  if (value.isValid()) {
    const auto key = value.toString().toStdString();
    const auto style =
        QMetaEnum::fromType<Qt::PenStyle>().keyToValue(key.data(), &ok);
    if (ok) {
      layer_pen.setStyle(static_cast<Qt::PenStyle>(style));
    }
  }
}

void DynamicDonut::saveDonutBeacons(QSettings *settings) {}

void DynamicDonut::loadDonutBeacons(QSettings *settings, QString prefix) {

  bool ok;
  auto wire_id = settings->value(prefix + "medea_id").toInt(&ok);
  if (!ok)
    return;
  auto rov_id = settings->value(prefix + "jason_id").toInt(&ok);
  if (!ok)
    return;

  auto wire_beacon = settings->value(prefix + "medea_beacon").toString();
  auto rov_beacon = settings->value(prefix + "jason_beacon").toString();

  if (wire_beacon.isEmpty() || rov_beacon.isEmpty()) {
    return;
  }

  this->rov_vehicle_id = rov_id;
  this->wire_vehicle_id = wire_id;

  this->rov_fix = rov_beacon;
  this->wire_fix = wire_beacon;
}

bool DynamicDonut::readDonutFromSettings(QSettings *settings, QString prefix) {
  bool ok = false;
  auto donut_count = settings->value(prefix + "donut_count").toInt(&ok);
  qCDebug(plugin_donut) << "donut count from settings is  " << donut_count;
  if (!ok)
    return false;

  auto depths = settings->value(prefix + "donut_depths").toString();
  auto inner_radii = settings->value(prefix + "donut_inner_radii").toString();
  auto outer_radii = settings->value(prefix + "donut_outer_radii").toString();

  // temporary string lists to check size of all lists matches donut count
  QStringList depth_list = depths.split(",");
  QStringList inner_list = inner_radii.split(",");
  QStringList outer_list = outer_radii.split(",");

  int vals[] = {donut_count, depth_list.size(), inner_list.size(),
                outer_list.size()};

  auto vals_begin = std::begin(vals);
  auto vals_end = std::end(vals);
  if (std::equal(vals_begin + 1, vals_end, vals_begin)) {
    qCWarning(plugin_donut) << "All sizes are equal. proceeding";
  } else {

    qCWarning(plugin_donut)
        << "Donut csv file in settings ini is corrupted. Not using";
    return false;
  }
  // reconstruct donut list in manner so it can be read just like the normal csv
  // file used by jason
  donutCount = 0;

  QString line = {depth_list[0] + "," + inner_list[0] + "," + outer_list[0]};
  processDonutCSVLine(line);

  for (int i = 1; i < donut_count && donutCount < MAX_DONUT_SIZE; i++) {
    line = {depth_list[i] + "," + inner_list[i] + "," + outer_list[i]};
    qCWarning(plugin_donut) << line;
    processDonutCSVLine(line);
  }

  return (donutCount > 2);
}

void DynamicDonut::saveSettings(QSettings &settings) {
  settings.setValue("persist", true);

  // add others settings from here to pass in
  this->saveDonutCsv(&settings);
  if (m_layer) {
    m_layer->saveSettings(&settings);
  }
}

QList<QAction *> DynamicDonut::pluginMenuActions() { return {}; }

bool DynamicDonut::processDonutFile(QString line) {
  QStringList myLineList = line.split(",");
  bool okConversion;
  if (myLineList.count() >= 3) {
    double depth = myLineList.at(0).toDouble(&okConversion);
    if (okConversion) {
      donut[donutCount].depth = depth;
    } else {
      return false;
    }
    double horizontal_min = myLineList.at(2).toDouble(&okConversion);
    if (!okConversion) {
      return false;
    }
    double horizontal_max = myLineList.at(1).toDouble(&okConversion);
    if (!okConversion) {
      return false;
    }
    if (horizontal_min < horizontal_max) {
      donut[donutCount].inner_radius = horizontal_min;
      donut[donutCount].outer_radius = horizontal_max;
    } else {
      donut[donutCount].outer_radius = horizontal_min;
      donut[donutCount].inner_radius = horizontal_max;
    }
    donutCount++;
  }
  return true;
}

/// connect ui slots
void DynamicDonut::connectUI() {
  connect(ui->loadDonutButton, &QPushButton::clicked, this,
          &DynamicDonut::openFile);

  connect(ui->exportDonutButton, &QPushButton::clicked, this, &DynamicDonut::exportDonutFile);

  connect(ui->minAppBox,
          static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), this,
          &DynamicDonut::changeMinimumApproach);

  connect(ui->maxDepBox,
          static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), this,
          &DynamicDonut::changeMaximumDeparture);

  connect(ui->enableManualCheck, &QCheckBox::stateChanged, this,
          &DynamicDonut::enableManualEntry);

}

bool DynamicDonut::openFile() {
  QString fileName = QFileDialog::getOpenFileName(
      this, tr("Load Donut File"), "/data", tr("Donut Files (*.csv)"));
  if (fileName.length() != 0) {
    QByteArray fileByteArray = fileName.toLatin1();
    this->fileOK = this->readDonutFile(fileByteArray.data());
    if (!fileOK) {
      QMessageBox::information(this, tr("Unable to read donut file"),
                               "error on read");
      return false;
    } else {
      return true;
    }
  }
  return false;
}

void DynamicDonut::exportDonutFile(){

    if (!fileOK){
        QMessageBox file_warning;
        file_warning.setIcon(QMessageBox::Critical);
        file_warning.setText("Error. No Donut values Found");
        file_warning.exec();
        return;
    }

    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Save Donut CSV"), "",
                                                    tr("Comma Separated Values (*.csv)"));

    if (fileName.isEmpty()){

        return;
    }

    if(!fileName.endsWith(".csv")){

        fileName.append(".csv");
    }

    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly)) {
        QMessageBox::information(this, tr("Unable to open file"),
                                 file.errorString());
        return;
    }
    QTextStream out(&file);
    out << "Delta Vert,Delta Horiz Min,Delta Horiz Max\n";
    for (int i = 0; i < donutCount; i++){
        out << donut[i].depth << "," << donut[i].inner_radius << "," << donut[i].outer_radius << "\n";
    }


}

void DynamicDonut::setNavestRemoteAddress(QHostAddress address) {
  m_navest_remote_address = address;
}

void DynamicDonut::setNavestRemoteTcpPort(int port) {
  m_navest_remote_tcp_port = port;
}

void DynamicDonut::saveDonutCsv(QSettings *settings) {
  if (donutCount <= 0)
    return;
  QString donut_depths;
  QString donut_inner_radii;
  QString donut_outer_radii;

  donut_depths.append(QString::number(donut[0].depth));
  donut_inner_radii.append(QString::number(donut[0].inner_radius));
  donut_outer_radii.append(QString::number(donut[0].outer_radius));

  // Starting at 2nd element. First element is written to QString
  // without preceeding comma
  for (int i = 1; i < donutCount; i++) {

    donut_depths.append(",").append(QString::number(donut[i].depth));
    donut_inner_radii.append(",").append(
        QString::number(donut[i].inner_radius));
    donut_outer_radii.append(",").append(
        QString::number(donut[i].outer_radius));
  }
  settings->setValue("csv/donut_count/", donutCount);
  settings->setValue("csv/donut_depths/", donut_depths);
  settings->setValue("csv/donut_inner_radii/", donut_inner_radii);
  settings->setValue("csv/donut_outer_radii/", donut_outer_radii);
}

bool DynamicDonut::readDonutFile(char *fileName) {
  QString donut_file_name = (QString)fileName;
  if (donut_file_name.isEmpty()) {
    qCWarning(plugin_donut, "donut filename empty");
    return false;
  } else {
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly)) {
      QMessageBox::information(this, tr("Unable to open donut file"),
                               file.errorString());
      return false;
    }

    QTextStream in(&file);
    donutCount = 0;

    QString line = in.readLine();
    processDonutCSVLine(line);
    while ((!line.isNull()) && (donutCount < MAX_DONUT_SIZE)) // MAXDONUTSIZE
    {
      line = in.readLine();
      processDonutCSVLine(line);
    }
    file.close();

    if (donutCount > 2) {
      return true;
    } else {
      qCWarning(plugin_donut, "Donut count is 2 or less. ERROR");
      return false;
    }
  }
}

void DynamicDonut::processDonutCSVLine(QString line) {

  QStringList myLineList = line.split(",");
  bool okConversion;
  if (myLineList.count() >= 3) {
    double inDepth = myLineList.at(0).toDouble(&okConversion);
    if (okConversion) {
      donut[donutCount].depth = inDepth;
    } else {
      return;
    }
    double horizontal_min = myLineList.at(2).toDouble(&okConversion);
    if (!okConversion) {
      return;
    }
    double horizontal_max = myLineList.at(1).toDouble(&okConversion);
    if (!okConversion) {
      return;
    }
    if (horizontal_min < horizontal_max) {
      donut[donutCount].inner_radius = horizontal_min;
      donut[donutCount].outer_radius = horizontal_max;
    } else {
      donut[donutCount].outer_radius = horizontal_min;
      donut[donutCount].inner_radius = horizontal_max;
    }
    donutCount++;
  }
}

void DynamicDonut::calculateDonut() {

  if (deltaDepth <= donut[0].depth) {
    theHole = donut[0].inner_radius;
    theCrust = donut[0].outer_radius;
    //"deltadepth is less or equal to donut[0] depth";
    return;

  } else if (deltaDepth >= donut[donutCount - 1].depth) {
    theHole = 0.5;
    theCrust = 1.5;
    //"deltadepth is greater or equal to last donut depth";
    return;
  } else {
    for (int step = 1; step < donutCount; step++) {
      if (deltaDepth < donut[step].depth) {
        double bot = donut[step - 1].depth;
        double top = donut[step].depth;

        double range = top - bot;
        double dz = deltaDepth - bot;
        double incFrac = dz / range;

        double holeInc =
            (donut[step].inner_radius - donut[step - 1].inner_radius) * incFrac;
        double crustInc =
            (donut[step].outer_radius - donut[step - 1].outer_radius) * incFrac;
        theHole = donut[step - 1].inner_radius + holeInc;
        theCrust = donut[step - 1].outer_radius + crustInc;

        return;
      }
    }
  }
}

void DynamicDonut::changeMinimumApproach(int val) {
  this->minApproachRad = val;
}

void DynamicDonut::changeMaximumDeparture(int val) { this->maxDepRad = val; }

void DynamicDonut::enableManualEntry(int itIsEnabled) {
  if (itIsEnabled) {
    useManual = true;
    ui->minAppBox->setEnabled(true);
    ui->maxDepBox->setEnabled(true);
  } else {
    useManual = false;
    ui->minAppBox->setEnabled(false);
    ui->maxDepBox->setEnabled(false);
  }
}

void DynamicDonut::donutTimeout() {
  QDateTime nowTime = QDateTime::currentDateTimeUtc();
  qint64 dt = deltaDepthTime.msecsTo(nowTime);
  double dtSecs = dt / 1000.0;
  ui->ageLabelValue->setText(QString::number(dtSecs, 'f', 0) + "s");
}

void DynamicDonut::handleVfrUpdate(int vehicleId, QString fix_source,
                                   double lon, double lat, double depth) {

  QDateTime currentTime = QDateTime::currentDateTimeUtc();
  if (vehicleId == wire_vehicle_id) {

    if (fix_source != wire_fix) {
      // "Fix source is not soln_usbl so returning";
      return;
    }
    // fixed on medea so set center here as well
    this->donutLatLon = QPointF(lat, lon);
    wireDepth = depth;
    wireDepthTime = currentTime;
    // return;
  } else if (vehicleId == rov_vehicle_id) {
    // jason/sentry/solnusbl
    if (fix_source != rov_fix) {
      //"Fix source is not soln_usbl so returning";
      return;
    }

    rovDepth = depth;
    rovDepthTime = currentTime;
  } else {
    // wrong vehicle id
    return;
  }

  if ((wireDepth == UNITIALIZED_DEPTH) || (UNITIALIZED_DEPTH == rovDepth)) {
    return;
  }
  if ((rovDepthTime.msecsTo(currentTime) < DEPTH_AGE_CRITERIA) &&
      (wireDepthTime.msecsTo(currentTime) < DEPTH_AGE_CRITERIA)) {
    // deltaDepth = rovDepth - wireDepth;
    deltaDepth = wireDepth - rovDepth;
    ui->deltaDepthLabelValue->setText(QString::number(deltaDepth, 'f', 1));
    deltaDepthTime = currentTime;
    //qCDebug(plugin_donut) << "file ok is set to " << fileOK;
    if (fileOK && !useManual) {
      calculateDonut();
      changeMinimumApproach(theHole);
      changeMaximumDeparture(theCrust);

      ui->holeLabelValue->setText(QString::number(theHole, 'f', 1));
      ui->crustLabelValue->setText(QString::number(theCrust, 'f', 1));

      // donut layer to add to map scene for drawing
      if (!m_layer) {
        qCDebug(plugin_donut) << "no donut layer. Creating ";
        Q_ASSERT(m_view != nullptr);
        auto my_scene = qobject_cast<dslmap::MapScene *>(m_view->mapScene());
        Q_ASSERT(my_scene != nullptr);

        m_layer = new DonutLayer(theHole, theCrust, donutLatLon);
        m_layer->setPenChange(layer_pen);
        // initial setup. When we get the first ever valid donut, construct
        // layer with it
        m_layer->setName("Donut");
        connect(this, &DynamicDonut::donutReady, m_layer,
                &DonutLayer::receiveDonutValues);

        my_scene->addLayer(m_layer);
        qCInfo(plugin_donut) << "donut layer added to scene";
      }
      // "emitting donut ready with values e

      else emit donutReady(theHole, theCrust, donutLatLon);
    }
  }
}

// slotted from UI
void DynamicDonut::setROVFix(QString fix) { this->rov_fix = fix; }
void DynamicDonut::setWireFix(QString fix) { this->wire_fix = fix; }
void DynamicDonut::setROVId(int id) { this->rov_vehicle_id = id; }
void DynamicDonut::setWireId(int id) { this->wire_vehicle_id = id; }

// public accessors
QString DynamicDonut::getROVFix() { return this->rov_fix; }
QString DynamicDonut::getMedeaFix() { return this->wire_fix; }
int DynamicDonut::getROVId() { return this->rov_vehicle_id; }
int DynamicDonut::getMedeaId() { return this->wire_vehicle_id; }

void DynamicDonut::donutLayerTest() {
  // TESTING TO CREATE DONUT MANUALLY IN PLACE
  Q_ASSERT(m_view != nullptr);
  auto my_scene = qobject_cast<dslmap::MapScene *>(m_view->mapScene());
  Q_ASSERT(my_scene != nullptr);
  // change to load layer from qsettings

  m_layer = new DonutLayer();
  m_layer->setName("Donut");
  connect(this, &DynamicDonut::donutReady, m_layer,
          &DonutLayer::receiveDonutValues);
  // const auto uuid = my_scene->addLayer(layer2);
  emit donutReady(60, 110, QPointF(77, 38));
  my_scene->addLayer(m_layer);
}

} // donut
} // plugins
} // navg
