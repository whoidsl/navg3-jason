/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef NAVG_DYNAMIC_DONUT_H
#define NAVG_DYNAMIC_DONUT_H

#include <navg/CorePluginInterface.h>

#include "DonutLayer.h"
#include <QAction>
#include <QHash>
#include <QHostAddress>
#include <QJsonObject>
#include <QLoggingCategory>
#include <QObject>
#include <QPointF>
#include <QTime>
#include <QWidget>
#include <memory>

// defines taken directly from navg2
#define MAX_DONUT_SIZE 256
#define DEPTH_AGE_CRITERIA 45000.0
#define UNITIALIZED_DEPTH -987654321.0
#define UNINITIALIZED_DEPTH -33000.0

namespace navg {
namespace plugins {
namespace donut {

typedef struct {
  double depth;
  double inner_radius;
  double outer_radius;
} donutDescriptor_t;

namespace Ui {
class DynamicDonut;
}

class DynamicDonut : public QWidget, public CorePluginInterface {
  Q_OBJECT
  Q_INTERFACES(navg::CorePluginInterface)
  Q_PLUGIN_METADATA(IID CorePluginInterface_iid FILE "DynamicDonut.json")

signals:

public:
  explicit DynamicDonut(QWidget *parent = nullptr);
  ~DynamicDonut() override;

  //
  // CorePluginInterface Implementation
  //
  void loadSettings(QSettings *settings) override;
  void saveSettings(QSettings &settings) override;

  void connectPlugin(const QObject *plugin,
                     const QJsonObject &metadata) override;
  void setMapView(QPointer<dslmap::MapView> view) override;
  QList<QAction *> pluginMenuActions() override;
  QList<QAction *> pluginToolbarActions() override { return {}; }

  bool processDonutFile(QString fileName);

  QString getROVFix();
  QString getMedeaFix();
  int getROVId();
  int getMedeaId();

protected slots:
  void setNavestRemoteAddress(QHostAddress address);
  void setNavestRemoteTcpPort(int port);

  bool openFile();
  void exportDonutFile();
  void changeMinimumApproach(int val);
  void changeMaximumDeparture(int val);
  void enableManualEntry(int itIsEnabled);

  void donutTimeout();

  void handleVfrUpdate(int vehicleId, QString fix_source, double lon,
                       double lat, double depth);
  void donutLayerTest();

  void setROVFix(QString);
  void setWireFix(QString);
  void setROVId(int);
  void setWireId(int);

signals:
  void donutReady(double, double, QPointF);

private:
  std::unique_ptr<Ui::DynamicDonut> ui;
  QPointer<DonutLayer> m_layer;

  QPointer<dslmap::MapView> m_view;
  void connectUI();
  bool readDonutFile(char *fileName);
  bool readDonutFromSettings(QSettings *settings, QString prefix);
  void saveDonutCsv(QSettings *settings);
  void saveDonutBeacons(QSettings *settings);
  void loadDonutBeacons(QSettings *settings, QString prefix);
  void loadQPenLayerSettings(QSettings *settings, QString prefix);
  void processDonutCSVLine(QString line);
  void calculateDonut();
  QString fileName;
  donutDescriptor_t donut[MAX_DONUT_SIZE];
  int donutCount;

  QHostAddress m_navest_remote_address = {};
  int m_navest_remote_tcp_port = 0;

  double rovDepth;
  double wireDepth;
  double deltaDepth;
  QDateTime rovDepthTime;
  QDateTime wireDepthTime;
  QDateTime deltaDepthTime;
  bool fileOK;
  QPointF donutLatLon;

  int minApproachRad;
  int maxDepRad;

  int useManual = false;
  
  //donut radius sizes
  double theHole;
  double theCrust;
  
  //navest vehicle ids
  int rov_vehicle_id;
  int wire_vehicle_id;

  //beacon fixes
  QString rov_fix;
  QString wire_fix;

  QTimer *donutTimer;
  QPen layer_pen;

  QSettings *m_settings;
};

Q_DECLARE_LOGGING_CATEGORY(plugin_donut)
} // donut
} // plugins
} // navg

#endif // NAVG_DYNAMIC_DONUT_H
