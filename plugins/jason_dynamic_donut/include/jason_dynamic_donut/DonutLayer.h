/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef NAVG_DONUT_LAYER_H
#define NAVG_DONUT_LAYER_H

#include "DonutPropertiesWidget.h"
#include <QColor>
#include <QLoggingCategory>
#include <QMetaEnum>
#include <QPainter>
#include <QPointF>
#include <QSettings>
#include <dslmap/MapLayer.h>

namespace navg {
namespace plugins {
namespace donut {

class Proj;
class DonutLayerPrivate;

class DonutLayer : public dslmap::MapLayer {
  Q_OBJECT
  Q_DECLARE_PRIVATE(DonutLayer)
public:
  struct DonutValues {
    double inner_radius;
    double outer_radius;
    QPointF center;
  };
  enum UpdateOption {
    UpdateNone = 0x0, //!< No-op
    UpdateDraw
  };
  Q_DECLARE_FLAGS(UpdateOptions, UpdateOption)
  DonutLayer(double inner_radius, double outer_radius, QPointF center,
             QGraphicsItem *parent = nullptr);
  bool update(UpdateOptions options);
  // virtual functions from maplayer
  void showPropertiesDialog();
  bool saveSettings(QSettings &settings) override;
  bool loadSettings(QSettings &settings) override;
  bool saveSettings(QSettings *settings);
  bool loadSettings(QSettings *settings);
  void savePenSettings(QPen *pen_ptr, QSettings *settings, QString prefix = "");
  static std::unique_ptr<DonutLayer> fromSettings(QSettings *settings);
  //
  QRectF boundingRect() const override;
  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
             QWidget *widget) override;

  explicit DonutLayer(QGraphicsItem *parent = nullptr);
  ~DonutLayer() override;
  void setProjection(std::shared_ptr<const dslmap::Proj> proj) override;
  QPen getPen();

public slots:
  void receiveDonutValues(double, double, QPointF);
  void drawDonut();
  void setPenChange(const QPen &pen);

signals:
  void beaconsChanged();

private:
  void testGeodeticConversion();
  DonutLayerPrivate *d_ptr;
  DonutValues donut;
};

// Q_DECLARE_LOGGING_CATEGORY(plugin_donut_layer)
}
}
}

#endif // NAVG_DONUT_LAYER_H