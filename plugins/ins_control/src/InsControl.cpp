/**
 * Copyright 2021 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by ssuman
//

#include "InsControl.h"
#include "ui_InsControl.h"
#include <navg/NavGMapScene.h>
#include <QMessageBox>
#include <QSettings>
#include <dslmap/Proj.h>

namespace navg {
namespace plugins {
namespace ins_control {

Q_LOGGING_CATEGORY(plugin_ins_control, "navg.plugins.ins_control")

InsControl::InsControl(QWidget* parent)
  : QWidget(parent)
  , ui(std::make_unique<Ui::InsControl>())
  , m_action_show_settings(new QAction("&Settings", this))
  , address("198.17.154.225")
  , lastRxd(QDateTime::currentDateTimeUtc().addSecs(-180))
{
  ui->setupUi(this);

  socket = new QUdpSocket(this);

  in_socket_port = 0;

  connect(m_action_show_settings,
          &QAction::triggered,
          this,
          &InsControl::showSettingsDialog);

  QObject::connect(socket,
                   SIGNAL(readyRead()),
                   this,
                   SLOT(dataPending()),
                   Qt::UniqueConnection);
  connect(
     ui->drBtn,
     &QPushButton::clicked,
     this,
     &InsControl::handleDrBtnClicked);

  connect(
     ui->insBtn,
     &QPushButton::clicked,
     this,
     &InsControl::handleInsBtnClicked);

  connect(
     ui->ahrsBtn,
     &QPushButton::clicked,
     this,
     &InsControl::handleAhrsBtnClicked);

  sacTimer = new QTimer(this);

  connect(sacTimer, &QTimer::timeout, this, &InsControl::ageCheck);

  sacTimer->start(1000); // Start the timer with a 1 sec period age check

  ui->drBtn->setToolTip("Set closed loop control to use doppler-based dead reckoning\n\nREQUIRES DOPPLER RESET");
  ui->insBtn->setToolTip("Set closed loop control to use INS-based solution\n\nREQUIRES CALIBRATED AND USBL-AIDED INS");
  ui->ahrsBtn->setToolTip("Set closed loop control to use Sparton AHRS solution\n\n");
}

InsControl::~InsControl()
{
  delete socket;
}

void
InsControl::handleDrBtnClicked()
{
  qDebug(plugin_ins_control) << "Dr Button Clicked";

  // Send "INSOUT 0" to navest navsensor_thread 
  const QByteArray data;// = QJsonDocument(jsonObj).toJson(QJsonDocument::Compact);

  QString command = "INSOUT 0";

  host_address.setAddress(address);
  socket->writeDatagram(command.toLocal8Bit(), host_address, to_socket_port);

  QMessageBox msgBox;
  msgBox.setText("Closed loop control set to DEAD RECKONING\n\nPlease do a DOPPLER RESET!");
  msgBox.exec();
  qDebug(plugin_ins_control)
    << "wrote datagram to "<< command << " to " << address << ":" << to_socket_port;

}

void
InsControl::handleInsBtnClicked()
{
  qDebug(plugin_ins_control) << "Ins Button Clicked";

  // Send "INSOUT 1" to navest navsensor_thread 
  QString command = "INSOUT 1";

  host_address.setAddress(address);
  socket->writeDatagram(command.toLocal8Bit(), host_address, to_socket_port);

  QMessageBox msgBox;
  msgBox.setText("Closed loop control set to AIDED INS\n\nPlease check dsros_INSStatus plugin!");
  msgBox.exec();

  qDebug(plugin_ins_control)
    << "wrote datagram "<< command << " to " << address << ":" << to_socket_port;
}

void
InsControl::handleAhrsBtnClicked()
{
  qDebug(plugin_ins_control) << "AHRS Button Clicked";

  // Send "INSOUT 1" to navest navsensor_thread 
  QString command = "INSOUT 2";

  host_address.setAddress(address);
  socket->writeDatagram(command.toLocal8Bit(), host_address, to_socket_port);

  QMessageBox msgBox;
  msgBox.setText("Closed loop control set to SPARTON AHRS Compass");
  msgBox.exec();

  qDebug(plugin_ins_control)
    << "wrote datagram "<<command<< " to " << address << ":" << to_socket_port;
}

void
InsControl::setMapView(QPointer<dslmap::MapView> view)
{
  m_view = view;
  n_view = qobject_cast<NavGMapView*>(view);
}

void
InsControl::connectPlugin(const QObject* plugin,
			  const QJsonObject& metadata)
{
  if (!plugin) {
    return;
  }

  const auto name =
    metadata.value("MetaData").toObject().value("name").toString();
  if (name.isEmpty()) {
    return;
  }
}

void
InsControl::loadSettings(QSettings* settings)
{

  auto value = settings->value("in_port");
  auto ok = false;
  if (value.isValid()) {
    const auto valid_val = value.toInt(&ok);
    if (ok) {
      in_socket_port = valid_val;
    }
  }

  if (!socket->bind(in_socket_port,
                    QUdpSocket::ShareAddress | QUdpSocket::ReuseAddressHint)) {
    qDebug(plugin_ins_control) << "could not bind to socket";
  } else {
    QObject::connect(socket,
                     SIGNAL(readyRead()),
                     this,
                     SLOT(dataPending()),
                     Qt::UniqueConnection);
  }

  auto value2 = settings->value("to_port");
  ok = false;
  if (value2.isValid()) {
    const auto valid_val = value2.toInt(&ok);
    if (ok) {
      to_socket_port = valid_val;
    }
  }

}

void
InsControl::showSettingsDialog()
{

  QScopedPointer<InsControlSettings> dialog(new InsControlSettings);
  //qDebug(plugin_ins_control) << "port value we are setting to dialog is " << in_socket_port;
  dialog->setPort(in_socket_port);
  //qDebug(plugin_ins_control) << "port to value we are setting to dialog is " << to_socket_port;
  dialog->setToPort(to_socket_port);

  dialog->setModal(true);
  if (!dialog->exec()) {
    return;
  }

  int temp = dialog->getPort();
  if (in_socket_port != temp) {
    in_socket_port = temp;
    socket->close();
    if (!socket->bind(in_socket_port,
                      QUdpSocket::ShareAddress |
                        QUdpSocket::ReuseAddressHint)) {
      qDebug(plugin_ins_control) << "could not bind to socket";
    } else {
      QObject::connect(socket,
                       SIGNAL(readyRead()),
                       this,
                       SLOT(dataPending()),
                       Qt::UniqueConnection);
    }
  }
  //qDebug(plugin_ins_control) << "port value received from dialog is " << in_socket_port;

  int temp2 = dialog->getToPort();
  if (to_socket_port != temp2) {
    to_socket_port = temp2;
    }
  //qDebug(plugin_ins_control) << "to port value received from dialog is " << to_socket_port;

}

void
InsControl::saveSettings(QSettings& settings)
{
  settings.setValue("persist", 1);
  settings.setValue("in_port", in_socket_port);
  settings.setValue("to_port", to_socket_port);
}

QList<QAction*>
InsControl::pluginMenuActions()
{
  return { m_action_show_settings };
}

void
InsControl::dataPending()
{
  QUdpSocket* theSocket = (QUdpSocket*)sender();

  while (theSocket->hasPendingDatagrams()) {

    QByteArray buffer(theSocket->pendingDatagramSize(), 0);
    theSocket->readDatagram(buffer.data(), buffer.size());
    //qDebug(plugin_ins_control) << "Bytes read: " << bytesRead;

    // Handle the "INSOUT STATUS 0/1/2" message sent with the INL logs by navest
    // to update the active button

    int enabled;
    int args = sscanf(buffer.data(), "%*s %*s %*s INSOUT STATUS %d", &enabled);
    //qDebug(plugin_ins_control) << "INSOUT status rx'ed: "<<enabled;
    if (args < 1)
      {
	//qDebug(plugin_ins_control) << "INSOUT STATUS parsing error "<<enabled;
      }
    else
      {
        //qDebug(plugin_ins_control) << "INSOUT STATUS "<<enabled;
	if (enabled == 1)
	  {
            ui->insBtn->setStyleSheet("QPushButton { color: white; background-color: green} ");
            ui->drBtn->setStyleSheet("QPushButton { color: white; background-color: red} ");
            ui->ahrsBtn->setStyleSheet("QPushButton { color: white; background-color: red} ");
	  }
  	else if (enabled == 2)
	{
            ui->insBtn->setStyleSheet("QPushButton { color: white; background-color: red} ");
            ui->drBtn->setStyleSheet("QPushButton { color: white; background-color: red} ");
            ui->ahrsBtn->setStyleSheet("QPushButton { color: white; background-color: green} ");  
	}
	else
	  {
            ui->insBtn->setStyleSheet("QPushButton { color: white; background-color: red} ");
            ui->drBtn->setStyleSheet("QPushButton { color: white; background-color: green} ");
            ui->ahrsBtn->setStyleSheet("QPushButton { color: white; background-color: red} ");
	  }
	lastRxd = QDateTime::currentDateTimeUtc();
      }
  }
}

void InsControl::ageCheck()
{
  QDateTime now = QDateTime::currentDateTimeUtc();

  //qDebug(plugin_ins_control) << "Age check";

  if (lastRxd.secsTo(now) > 15)
    {
      ui->insBtn->setStyleSheet("QPushButton { border:1px solid yellow} ");
      ui->drBtn->setStyleSheet("QPushButton { border:1px solid yellow} ");
      ui->ahrsBtn->setStyleSheet("QPushButton { border:1px solid yellow} ");
    }
}

} // ins_control
} // plugins
} // navg
