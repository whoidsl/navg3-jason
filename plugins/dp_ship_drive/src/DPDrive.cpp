/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#include "DPDrive.h"
#include "ui_DPDrive.h"
#include <dslmap/MapView.h>
#include <QAction>
#include <QSettings>
#include <QTimer>
#include <QDoubleSpinBox>



namespace navg_jason {
namespace plugins {
namespace dp_drive {

Q_LOGGING_CATEGORY(plugin_dp_drive, "navg_jason.plugins.dp_drive")

DPDrive::DPDrive(QWidget *parent)
  : QWidget(parent), ui(std::make_unique<Ui::DPDrive>())
  , m_layer(nullptr)
{
    ui->setupUi(this);

    // This plugin is significantly different from navg2 dp plugin.
    // In navg2 all the dp math and i/o feeds was done in the gui. This has several
    // disadvantages:
    // 1. Only one instance of the gui can use dp control
    // 2. It is essentially unusable over satellite high latency links, which
    //    would make impossible having remote navigator capabilities
    // 3. Philosophically, all the nav computations should reside in navest
    //    or a ros plugin of some sort and the gui is only a tool to interact with that
    // So, this new plugin will just be an interface to the underlying nav engine
    // to address all the shortcomings listed above
    reference_speed = 0;

    dp_update_interval = 1.0;
    dp_timer = new QTimer;   //delete in destructor
    dp_timer->setInterval((int)(dp_update_interval * 1000.0));
    disable_init_timer = new QTimer;   //delete in destructor
    disable_init_timer->setInterval((int)(20000.0));
    dp_type = NO_DP;
    last_ship_local_pos = QPointF(0,0);
    current_ship_local_pos = QPointF(0,0);
    ui->start_dp_button->setEnabled(false);
    ui->no_dp_radio->setChecked(true);
    m_markers.reserve(2);
    lastRxStatusDT = QDateTime::currentDateTimeUtc();

    m_range = ui->rangeSpn->value();
    m_bearing = ui->bearingSpn->value();
}

DPDrive::~DPDrive()
{
  dp_timer->stop();
  disable_init_timer->stop();
  delete dp_timer;
  delete disable_init_timer;
  delete dpSocket;
}

void DPDrive::setMapView(QPointer<dslmap::MapView> view) {
    // parent view
    m_view = view;

    // new cast view contained in this object
    nview = qobject_cast<navg::NavGMapView *>(view);
    if (!nview) {
        qCWarning(plugin_dp_drive) << "nview not valid";
        return;
    }

    connect(nview, &navg::NavGMapView::mouseDoubleClickedOnScene, this,
            &DPDrive::handleSceneMousePress );

}

void DPDrive::connectPlugin(const QObject *plugin, const QJsonObject &metadata) {
    if (!plugin) {
        return;
    }


    const auto name =
            metadata.value("MetaData").toObject().value("name").toString();
    if (name.isEmpty()) {
        return;
    }
    if (name == "navest") {
        if (!connect(plugin, SIGNAL(remoteAddressChanged(QHostAddress)), this,
                     SLOT(setNavestRemoteAddress(QHostAddress)))) {
            qCCritical(plugin_dp_drive, "Unable to connect to signal: "
                                        "Navest::remoteAddressChanged("
                                        "QHostAddress)");
            Q_ASSERT(false);
        } else {
            qCDebug(plugin_dp_drive, "connected to navest");
        }

        connect(plugin, SIGNAL(messageReceived(NavestMessageType, QVariant)), this,
                SLOT(updateFromNavestMessage(NavestMessageType, QVariant)));
        connectUI();
    }



}

void DPDrive::loadSettings(QSettings *settings) {

    if (settings == nullptr) {
      return;
    }

    QString address;
    address = settings->value("DP_host_address", "127.0.0.1").toString();
    dpHostAddress.setAddress(address);
    dpHostInSocketNumber = settings->value("port_from_DP", "5111").toInt();
    dpHostOutSocketNumber = settings->value("port_to_DP", "5112").toInt();
    dpEnabled = settings->value("DP_enabled", false).toBool();

    dpSocket = new QUdpSocket(this); // Delete in destructor
    if (!dpSocket->bind(dpHostInSocketNumber, QUdpSocket::ShareAddress |QUdpSocket::ReuseAddressHint))
      {
	qDebug(plugin_dp_drive) << "Could not bind dp socket to " << dpHostInSocketNumber;
      }

    // Connect the socket receiving status updates from the nav engine
    QObject::connect(dpSocket,
		     SIGNAL(readyRead()),
		     this,
		     SLOT(dpSocketDataPending()),
		     Qt::UniqueConnection);

    // Start the DP timer to send user requests to the nav engine
    dp_timer->start();

    return;
  }

void DPDrive::saveSettings(QSettings &settings) {
    settings.setValue("DP_host_address", dpHostAddress.toString());
    settings.setValue("port_from_DP", dpHostInSocketNumber);
    settings.setValue("port_to_DP", dpHostOutSocketNumber);
    settings.setValue("DP_enabled", dpEnabled);
    return;
}

void DPDrive::connectUI(){


    connect(dp_timer,&QTimer::timeout,this,&DPDrive::dpIntervalTimeout);
    connect(disable_init_timer,&QTimer::timeout,this,&DPDrive::dpDisableInitTimeout);
    connect(ui->northButton, &QPushButton::pressed, this, &DPDrive::handleDirectionButton);
    connect(ui->southButton, &QPushButton::pressed, this, &DPDrive::handleDirectionButton);
    connect(ui->eastButton, &QPushButton::pressed, this, &DPDrive::handleDirectionButton);
    connect(ui->westButton, &QPushButton::pressed, this, &DPDrive::handleDirectionButton);

    ui->range_change_combo->addItem("1.0", QVariant(1.0));
    ui->range_change_combo->addItem("10.0", QVariant (10.0));
    ui->range_change_combo->addItem("100.0", QVariant (100.0));
    ui->range_change_combo->addItem("1000.0", QVariant(1000.0));

    connect(ui->rangeSpn, static_cast<void (QDoubleSpinBox::*)(double)>(
                &QDoubleSpinBox::valueChanged), this, &DPDrive::handleRangeSpn);

    connect(ui->bearingSpn, static_cast<void (QDoubleSpinBox::*)(double)>(
                &QDoubleSpinBox::valueChanged), this, &DPDrive::handleBearingSpn);

    connect(ui->speed_dial_spin, static_cast<void (QDoubleSpinBox::*)(double)>(
                &QDoubleSpinBox::valueChanged), this, &DPDrive::handleSpeedDialSpin);

    connect(ui->enable_dp_check, &QCheckBox::stateChanged, this, &DPDrive::handleEnableDPCheck);
    connect(ui->start_dp_button, &QPushButton::clicked, this , &DPDrive::handleStartDPButton);
    connect(ui->goalRangeBearingBtn, &QPushButton::clicked, this, &DPDrive::handleGoalRangeBearingBtn);
    connect(ui->goal_ref_button, &QPushButton::clicked, this, &DPDrive::handleGoalRefButton);
    connect(ui->goal_cursor_button, &QPushButton::clicked, this, &DPDrive::handleGoalCursorButton);
    connect(ui->no_dp_radio,&QRadioButton::toggled, this, &DPDrive::handleDPSelectionRadios );
    connect(ui->dgps_radio,&QRadioButton::toggled, this, &DPDrive::handleDPSelectionRadios );

    connect(ui->goal_color_button, &dslmap::ColorPickButton::colorChanged,
            [=](const QColor color) { emit goalColorChanged(color); });
    connect(ui->ref_color_button, &dslmap::ColorPickButton::colorChanged,
            [=](const QColor color) { emit refColorChanged(color); });

    connect(this, &DPDrive::goalColorChanged, this,
            &DPDrive::setGoalColor);
    connect(this, &DPDrive::refColorChanged, this,
            &DPDrive::setRefColor);

    //ui->no_dp_radio->setStyleSheet("QRadioButton { background-color: transparent; color: black; }");
    //ui->dgps_radio->setStyleSheet("QRadioButton { background-color: lightGreen; color: black; }");
}

void DPDrive::updateFromNavestMessage(navg::NavestMessageType type,
                                      QVariant message) {

    if (type == navg::NavestMessageType::VFR) {

        int vehicle_id = message.value<navg::NavestMessageVfr>().vehicle_number;

        //vehicle 2 is ship
        if (vehicle_id == 2){

            last_ship_local_pos.setX(current_ship_local_pos.x());
            last_ship_local_pos.setY(current_ship_local_pos.y());
        }
        else
        {
            return;
        }

    }
}



QList<QAction *> DPDrive::pluginMenuActions() { return {}; }

void DPDrive::handleDirectionButton(){

    //can handle request based on sender. all do similar things
    //or use additional info passed from slot
    qCDebug(plugin_dp_drive) << sender();

    double step_index = ui->range_change_combo->currentIndex();
    double step_size = ui->range_change_combo->currentData().toDouble();

    qCDebug(plugin_dp_drive) << "step index is " << step_index;
    qCDebug(plugin_dp_drive) << "step size is " << step_size;

    // Send message to nav engine telling it to increment goal position
    // The message is in xy step and the nav engine will compute
    // a new lat lon goal based on that

    QPushButton *selected_button = (QPushButton *)sender();
    if (selected_button == ui->northButton){
        incrementGoal(0, step_size);
    }
    else if (selected_button == ui->southButton){
        incrementGoal(0, -step_size);
    }
    else if (selected_button == ui->eastButton){
        incrementGoal(step_size,0);
    }
    else if (selected_button == ui->westButton){
        incrementGoal(-step_size,0);
    }
}

void DPDrive::handleSpeedDialSpin(double value){
    changeSpeed(value);
}

void DPDrive::handleBearingSpn(double value){
  m_bearing = value;
}

void DPDrive::handleRangeSpn(double value){
  m_range = value;
}

void DPDrive::handleRangeComboChoice(){

}

void DPDrive::handleSceneMousePress(QPointF cursor){

    auto map_scene = m_view->mapScene();

    if (!map_scene) {

        return;
    }
    double lon = cursor.x();
    double lat = cursor.y();
    auto proj = map_scene->projection();
    //get proj from scene first
    proj->transformToLonLat(1,&lon,&lat);

    qDebug(plugin_dp_drive) << "Target lat: " << lat << " lon: " << lon;

    cursor_lon = lon;
    cursor_lat = lat;
}

void DPDrive::changeSpeed(double speed){

    if(speed > MAX_REF_SPEED)
    {
        reference_speed = MAX_REF_SPEED;
    }
    else if(speed < 0.0)
    {
        reference_speed = 0.0;
    }
    else{
        reference_speed = speed;
    }
}

void DPDrive::handleEnableDPCheck(bool checked){

  (checked == true) ? ui->start_dp_button->setEnabled(true) : ui->start_dp_button->setEnabled(false);
  if (checked == true)
    {
      disable_init_timer->start(20000);
    }
}

void DPDrive::handleStartDPButton(){
    if (ui->enable_dp_check->isChecked()){
      // send a message to init dp
      initializeDp();

      // Create layer must be called only after a projection already exists. here it's
      // very likely that the projection already exists.
      // Maybe we should add a signal emitted by the core parts that is emitted
      // when a projection is made available?
      createLayer();

      // This is just a test marker      
      //auto marker = m_layer->addLonLatPoint(-130.1, 45.9, "GOAL");
      //m_markers[0] = marker;
      //auto marker2 = m_layer->addLonLatPoint(-130.1, 46.0, "REF");
      //m_markers[1] = marker2;
      //m_layer->removePoint(m_markers[1]);
      //auto marker3 = m_layer->addLonLatPoint(-130.1, 46.1, "REF");
      //m_markers[1] = marker3;

    }
    else return;

}

void DPDrive::handleGoalRefButton(){

}

void DPDrive::handleGoalCursorButton(){

}

void DPDrive::handleGoalRangeBearingBtn(){
  //gotoRangeBearing(range, bearing);
}

void DPDrive::handleDPSelectionRadios(int state){
    QRadioButton *selection = (QRadioButton *)sender();

      if (selection == ui->dgps_radio)
         {
	   setDPType(FAUX_GPS);
         }

      else if (selection == ui->no_dp_radio)
         {
	   setDPType(NO_DP);
         }
}

void DPDrive::setGoalColor(QColor color){

}

void DPDrive::setRefColor(QColor color){

}

void DPDrive::setDPType(DP_TYPE type){
     dp_type = type;
}

void DPDrive::dpDisableInitTimeout(){
  if (ui->enable_dp_check->isChecked())
    {
      ui->enable_dp_check->setChecked(false);
      disable_init_timer->stop();
    }
}

void DPDrive::createLayer()
{
  dslmap::MapScene* scene = qobject_cast<dslmap::MapScene*>(m_view->mapScene());
  Q_ASSERT(scene != nullptr);

  if (m_layer != nullptr) {
    qCWarning(plugin_dp_drive, "Already had old layer; removing");
    auto layer = scene->removeLayer(m_layer->id());
    delete layer;
  }

  m_layer = new dslmap::SymbolLayer();

  //auto scene_projection = scene->projection();
  //m_layer->setProjection(scene_projection);

  auto target = scene->addLayer(m_layer);
  if (target.isNull()) {
    qCCritical(plugin_dp_drive, "Unable to add layer to scene.");
    return;
  }
  qCDebug(plugin_dp_drive, "Added layer to scene.");

  m_layer->setName("DP goal");

  int marker_size = 20;
  m_layer->setShape(dslmap::ShapeMarkerSymbol::Shape::Circle, marker_size);
  m_layer->setScaleInvariant(true);

  auto symbol_ptr = m_layer->symbolPtr();

  symbol_ptr->setOutlineColor(Qt::red);
  symbol_ptr->setFillColor(Qt::red);
  symbol_ptr->setFillStyle(Qt::SolidPattern); // defaults to Qt::NoBrush
  symbol_ptr->setLabelColor(Qt::white);

  m_layer->setLabelVisible(true);
  m_layer->setVisible(true);

}

bool DPDrive::hasLayer()
{
  return !(m_layer == nullptr);
}

////////// THE FOLLOWING ARE INTERFACE IMPLEMENTATIONS

void DPDrive::dpSocketDataPending()
{
   QUdpSocket *theSocket = (QUdpSocket *)sender();

   while(theSocket->hasPendingDatagrams())
      {

         QByteArray	buffer(theSocket->pendingDatagramSize(),0);
         qint64 bytesRead = theSocket->readDatagram(buffer.data(),buffer.size());
	 qDebug(plugin_dp_drive) << "Bytes read: " << bytesRead;

	 // Here receive the status updates from the nav engine and update user display status:
	 // 1. whether DP is actually enabled or disabled
	 // 2. error remaining to position goal
	 // 3. Active mode: 0 - move at range and bearing, 1 - move to position
	 // 4. actual reference speed
	 // 5. actual lat lon goal
	 // 6. actual lat lon reference (this moves smoothly towards the goal at the reference speed)
	 // Update plugin controls color based on status match with request

	 int l_enabled;
	 double l_x_err;
	 double l_y_err;
	 int l_active_mode;
	 double l_ref_speed;
	 double l_goal_lon;
	 double l_goal_lat;
	 double l_ref_lon;
	 double l_ref_lat;
	 int args = sscanf(buffer.data(), "DPS %d %lf %lf %d %lf %lf %lf %lf %lf",
			   &l_enabled,
			   &l_x_err,
			   &l_y_err,
			   &l_active_mode,
			   &l_ref_speed,
			   &l_goal_lon,
			   &l_goal_lat,
			   &l_ref_lon,
			   &l_ref_lat);
	 if (args < 9)
	   {
	     qDebug(plugin_dp_drive) << "DPS parsing error";
	     return;
	   }
	 else
	   {
	     // Update the last rx message
	     lastRxStatusDT = QDateTime::currentDateTimeUtc();
	     if (l_enabled)
	       {
		 ui->no_dp_radio->setStyleSheet("QRadioButton { background-color: transparent; color: black; }");
		 ui->dgps_radio->setStyleSheet("QRadioButton { background-color: lightGreen; color: black; }");
	       }
	     else
	       {
		 ui->no_dp_radio->setStyleSheet("QRadioButton { background-color: lightGreen; color: black; }");
		 ui->dgps_radio->setStyleSheet("QRadioButton { background-color: transparent; color: black; }");
	       }
	     ui->exLbl->setText("ex: " + QString::number(l_x_err));
	     ui->eyLbl->setText("ey: " + QString::number(l_y_err));
	     if (m_markers[MARKER_GOAL] != nullptr)
	       {
		 m_layer->removePoint(m_markers[MARKER_GOAL]);
		 auto marker = m_layer->addLonLatPoint(l_goal_lon, l_goal_lat, "GOAL");
		 m_markers[MARKER_GOAL] = marker;
	       }
	     if (m_markers[MARKER_REF] != nullptr)
	       {
		 m_layer->removePoint(m_markers[MARKER_REF]);
		 auto marker = m_layer->addLonLatPoint(l_goal_lon, l_goal_lat, "REF");
		 m_markers[MARKER_REF] = marker;
	       }
	   }
      }
}

void DPDrive::dpIntervalTimeout(){

  // make the synchronous user request messages for the nav engine and
  // send it to it. Things to put in it:
  // 1. DP enabled/disabled flag
  // 2. Requested ref speed
  // 3. Requested Mode: 0 - move at range and bearing, 1 - move to position
  // 4. Target range and bearing
  // 5. Target position (lon, lat)
  // The nav engine should keep track of the last request message received and
  // automatically disable DP output if it stops receiving messages for more
  // than a certain amount of time, as that would mean that the user ability
  // to control the dp system is inhibited
  QString rdp_os;

  rdp_os = QString("DP RDP ") + QString::number((dp_type == NO_DP) ? 0 : 1) + QString("\r\n");

  qDebug(plugin_dp_drive) << "DP request: " << rdp_os;


  // Update staleness display
  QDateTime lastTimer = QDateTime::currentDateTimeUtc();
  qDebug(plugin_dp_drive) << "Last status rxd: " << lastRxStatusDT.secsTo(lastTimer);
  if (lastRxStatusDT.secsTo(lastTimer) > 10)
    {
      ui->no_dp_radio->setStyleSheet("QRadioButton { background-color: yellow; color: black; }");
      ui->dgps_radio->setStyleSheet("QRadioButton { background-color: yellow; color: black; }");
    }

}

void DPDrive::incrementGoal(double x_amount, double y_amount){
  QString xyGoalChange = "DP XYS " + QString::number(x_amount) + " " + QString::number(y_amount);
  dpSocket->writeDatagram(xyGoalChange.toLocal8Bit(), dpHostAddress, dpHostOutSocketNumber);
}

void DPDrive::gotoRangeBearing(double range, double bearing){
  QString rangeBearing = "DP RBG " + QString::number(m_range) + " " + QString::number(m_bearing);
  dpSocket->writeDatagram(rangeBearing.toLocal8Bit(), dpHostAddress, dpHostOutSocketNumber);
}

void DPDrive::gotoLonLat(double lon, double lat){
  QString llGoal = "DP GLL " + QString::number(lon,'f',5) + " " + QString::number(lat,'f',5);
  dpSocket->writeDatagram(llGoal.toLocal8Bit(), dpHostAddress, dpHostOutSocketNumber);
}

void DPDrive::initializeDp(void){

  // Send message to nav engine to initialize DP. Actions to take there:
  // 1. Set xy projection origin to current ship position
  // 2. Set reference to current ship position
  // 3. Set goal to current reference

  QString dpInit = "DP INI";
  dpSocket->writeDatagram(dpInit.toLocal8Bit(), dpHostAddress, dpHostOutSocketNumber);
}



} // dp_drive
} // plugins
} // navg_jason
