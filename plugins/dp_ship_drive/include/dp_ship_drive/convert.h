/********
* file: convert.h
* date: 12Nov89
* vers: 1.00
* orig: Steve Gegg

   April 16 2001 LLW updated some trig constants

* include file for use in data processing and plotting routines
  contains miscellaneous conversion defines
********/

#ifndef _CONVERSIONS
#define _CONVERSIONS

#define CMS_INCH	2.54	/* cms/inch */
#define METERS_FOOT 	0.3048	/* meters/foot */
#define METERS_NMILE	1852.0	/* meters/nautical mile */
#define METERS_FATHOM	1.8288	/* meters/fathom (straight conversion) */
#define UMETERS_UFATHOM	1.8750	/* unc mtrs/unc fthm - accounts for */
/* snd vel of 1500m/sec to 800fm/sec */

/* April 16 2001 LLW updated value here */
#define PI		 3.1415926535897932384626433832795		/* PI */
#define TWO_PI		(2.0 * PI)		/* 2*PI */

/* April 16 2001 LLW updated value here */
#define RADIANS		(57.295779513082320876798154814105)		/* degrees/radian */
#define RTOD         RADIANS
#define DTOR         (1.0/RTOD)

#define KNOTS_TO_METERS_PER_SECOND 0.514

#define SECS_HOUR 3600
#define DEG_TO_RADIANS(a)	(a/RADIANS)	/* degrees (a) to radians */
#define RAD_TO_DEGREES(a)	(a*RADIANS)	/* radians (a) to degrees */
#define DEGMIN_TO_DECDEG(a,b) ((a)+(b/60.0))	/* deg,min to decimal degrees */
#define DEGMIN_TO_SECS(a,b) ((a*3600.0)+(b*60.0)) /* deg,min to seconds */
#define MSEC_TO_KNOTS(a)	((a/METERS_NMILE)*SECS_HOUR)
#define KNOTS_TO_MSEC(a)	((a*METERS_NMILE)/SECS_HOUR)
#define FEET_TO_METERS(a)	(a * METERS_FOOT)

#define	SECS_TO_MSECS		1000.0

#define	DEGREES_PER_METER 0.000008999


/* coordinate translation */
struct ORIGIN {		/* ORIGIN data structure */
  int coord_system;
  double latitude_degs,longitude_degs;
  double xoffset_mtrs,yoffset_mtrs;
  double rotation_angle_degs,rms_error;
};

/* coordinate translation options */
#define XY_TO_LL		1
#define LL_TO_XY		2
#define LL_TO_LL		3

/* coordinate system types */
#define GPS_COORDS_WGS84	1
#define GPS_COORDS_C1866	2
#define LORANC_COORDS		3
#define SURVEY_COORDS		4
#define TRANSIT_COORDS		5

/* coordinate system type messages */
#define GPS_DATA_WGS84		"GPS, WGS'84 \n"
#define GPS_DATA_C1866		"GPS, Clarke 1866 \n"
#define LORC_DATA		"LORANC Data\n"
#define TRANSIT_DATA		"TRANSIT Satellite Data\n"
#define SURVEY_DATA		"SURVEYED Benchmark Data\n"


#define inrange(a, b, c) (((a)>=(b)) && ((a)<=(c)))

#define  PRESSURE_TO_DEPTH .9930   // decibars to meters, from unesco page

#endif

