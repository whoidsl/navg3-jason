Writing a NavG plugin ... J Vaccaro

I. Motivation

In order to limit the risk of software changes, the NavG codebase
is split into several libraries.

dslmap is a library for the pieces which make up NavG.
This code should be highly stable and thoroughly tested.

navg is the main topside program which intakes and displays
vehicle information. It instantiates map objects, connects
plugins, and saves/loads configurable information. It depends
on dslmap, and looks for plugins built at startup.

plugins are defined individually, and can depend on code from
navg, dslmap, and other plugins. Plugins communicate exclusively
through Qt signal/slot connections. Vehicle-specific code should
live here. Through navg, plugins can be instantiated, hidden, and
closed without restarting the whole program.

Each plugin should solve a single limited-scope problem. They
each subclass navg::CorePluginInterface, and if they also subclass
QWidget, they can be displayed as QDockWidgets in navg.

II. Design choices

What do you want from this plugin? Who do you expect to use it?
What existing features should be used in the plugin?

Plugins should solve a single limited-scope problem. Namespacing
should indicate the scope of what the plugin does, who uses it,
and sometimes on what other code it depends. If a plugin is
vehicle-specific, then start with the vehicle name.

What interaction would users have with information and controls associated
with the plugin? How will users know when the plugin is working?

Plugins can have (but do not require) a primary QWidget ui. If your
plugin subclassed QWidget (or another QWidget class) then it will be loaded
into a QDockWidget for configurability. Menu actions through the "plugins"
navg menu or the navg toolbar provide access to dialogs for editing properties
or displaying information.

How will the plugin interact with navg and other plugins? Will the plugin
include code from other libraries?

This consideration may take some research of the existing codebase, in
particular other plugins and dslmap. E.g. if you want your plugin to define
and load a custom layer based on incoming navest data, then you'll need to
receive data via a connection to navg::plugins::Navest, include the header
dslmap/MapLayer.h to subclass dslmap::MapLayer, and finally access the
scene to call dslmap::MapScene::addLayer().

III. Create the plugin template

Create the directory structure
Here is a minimal structure for a plugin called "template"
Use an actual working plugin for more complex examples and
for guidance writing tje contents of each file. sentry_usbl shows a
lot of necessary functionality.
+ plugins
   CMakeLists.txt
  + template
     CMakeLists.txt
    + cmake
       navg_template-config.cmake.in
    + include
      + template
         Template.h
         Template.json
    + src
       CMakeLists.txt
       Template.cpp
    + tests
       runner.cpp
       test_plugin_template.cpp

  + (other plugins...)

Subclass navg::CorePluginInterface and override...
 - void loadSettings(QSettings* settings)
 - void saveSettings(QSettings& settings)
 - void connectPlugin(const QObject* plugin,
                      const QJsonObject& metadata)
 - void setMapView(QPointer<dslmap::MapView> view)
 - QList<QAction*> pluginMenuActions()
 - QList<QAction*> pluginToolbarActions()

Add your plugin directory to plugins/CMakeLists.txt
  add_subdirectory(template)

III. Write the plugin

Naming/namespacing
Put all code in a namespace e.g. navg::plugins::template::Template
This will help avoid conflicts between the codebases for different
vehicles.

Connections to main NavG program
If the plugin needs to connect to the main navg program, it must
do so via the MapView pointer from setMapView(). Store this pointer
locally, and then use it to access the scene to add layers

Connections to other NavG plugins
All such connections should be made in connectPlugin(). When
the plugin is instantiated in navg, this function will be run
pairwise with each other plugin. Check out sentry_hdg_depth_alt
for an example with multiple connections to other plugins.

Saving and loading
When the navg program is saved, so too will the plugins that are
currently active. These will write to a human readable ini file
in a section with the same name as the plugin. On startup, if there is
a section with the name of the plugin, then the plugin should load with
those saved settings.

IV. Testing and debugging
Here's an incomplete list of some common annoyances and how to address them.
Please add to this list!

plugin is not compiling
  Confirm that you have correctly subclassed CorePluginInterface.
  Confirm that your included header files are handled correctly.

navg crashes when the plugin loads
  Check the error console, but this sounds like a signals/slots error.
  Check out this very helpful debugging checklist:
  https://samdutton.wordpress.com/2008/10/03/debugging-signals-and-slots-in-qt/

plugin does not automatically restart after saving/reloading
  Make sure that you are saving something. Even a dummy value,
  like settings.setValue("plugin_value", 1) will do. Basically, if
  there's nothing to save, then the ini file won't have a section
  for the plugin, and it won't launch on reload.

The navg stylesheet does not propogate to all widgets.
  If you have an unbroken chain of Qt parent-child hierarchy, then your
  widgets will use the same stylesheet as navg. If your main plugin subclasses
  QWidget, then set the parent of your other widgets/dialogs to the main plugin.
  Otherwise, you can set the parent as the MapView in setMapView().