/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef NAVG_DVL_SWITCH_H
#define NAVG_DVL_SWITCH_H

#include <navg/CorePluginInterface.h>

#include <QAction>
#include <QDoubleValidator>
#include <QHash>
#include <QHostAddress>
#include <QJsonObject>
#include <QLoggingCategory>
#include <QObject>
#include <QTime>
#include <QWidget>
#include <QtGlobal>
#include <memory>
#include <QMouseEvent>
#include <navg/plugins/navest/Messages.h>
#include <navg/plugins/navest/Navest.h>

namespace navg {
namespace plugins {
namespace jason {

namespace Ui {
class DvlSwitch;
}

class DvlSwitch : public QWidget, public CorePluginInterface {
  Q_OBJECT
  Q_INTERFACES(navg::CorePluginInterface)
  Q_PLUGIN_METADATA(IID CorePluginInterface_iid FILE "DvlSwitch.json")

signals:

public:
  explicit DvlSwitch(QWidget *parent = nullptr);
  ~DvlSwitch() override;

  //
  // CorePluginInterface Implementation
  //
  void loadSettings(QSettings *settings) override;
  void saveSettings(QSettings &settings) override;
  
  //connect to navest plugin
  void connectPlugin(const QObject *plugin,
                     const QJsonObject &metadata) override;

  //receive mapview reference. Actually a navg::NavGMapView object.
  void setMapView(QPointer<dslmap::MapView> view) override;
  QList<QAction *> pluginMenuActions() override;
  QList<QAction *> pluginToolbarActions() override { return {}; }


public slots:
  void handleSceneMousePress(QPointF cursor);
 
protected slots:
  void updateFromNavestMessage(NavestMessageType type, QVariant message);
  void sendDopplerToUse();
  void sendDopplerOffset();
  void sendDvlReset(QPointF fix);
  void setNavestRemoteAddress(QHostAddress address);
  void handleDopplerChoice(bool is_checked);
  void handleResetButton();
  void handleLastFixButton();
  void handleResetComboChanged(int index);

signals:
  void sendNavestMessage(const QByteArray &message);

private:
  NavGMapView *nview;
  int doppler_choice; // 0 for rdi, 1 for syrinx. defaults at 0 on creation.
                      // manipulated by radio buttons
  QDoubleValidator *offset_validator;
  std::unique_ptr<Ui::DvlSwitch> ui;
  QPointer<dslmap::MapView> m_view;
  QHostAddress m_navest_remote_address;

};

Q_DECLARE_LOGGING_CATEGORY(plugin_dvl_switch)
} // jason
} // plugins
} // navg

#endif // NAVG_DVL_SWITCH_H
