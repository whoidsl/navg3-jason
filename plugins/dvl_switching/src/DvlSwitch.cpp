/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#include "DvlSwitch.h"
#include "ui_DvlSwitch.h"
#include <QAction>
#include <QSettings>
#include <QTimer>
#include <dslmap/MapView.h>
#include <navg/NavGMapView.h>
#include <dslmap/MapScene.h>
#include <dslmap/Proj.h>

namespace navg {
namespace plugins {
namespace jason {

Q_LOGGING_CATEGORY(plugin_dvl_switch, "navg.plugins.dvl_switch")

DvlSwitch::DvlSwitch(QWidget *parent)
    : QWidget(parent), ui(std::make_unique<Ui::DvlSwitch>()) {
    ui->setupUi(this);
    offset_validator = new QDoubleValidator(-359.99, 359.99, 2, ui->offset_entry);
    offset_validator->setNotation(QDoubleValidator::StandardNotation);
    ui->offset_entry->setValidator(offset_validator);
    ui->rdi_radio->setChecked(true); // default as selected doppler
    this->doppler_choice = 0;        // default to rdi
    setMouseTracking(true);

    ui->reset_combo->addItem("Cursor Position", QVariant(QPointF{}));
    ui->reset_combo->addItem("Last USBL Fix", QVariant(QPointF{}));
    connect(ui->reset_combo, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, &DvlSwitch::handleResetComboChanged);
    //static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged);
}

DvlSwitch::~DvlSwitch() = default;

void DvlSwitch::setMapView(QPointer<dslmap::MapView> view) {

    // parent view
    m_view = view;

    // new cast view contained in this object
    nview = qobject_cast<NavGMapView *>(view);
    if (!nview) {
        qCWarning(plugin_dvl_switch) << "nview not valid";
        return;
    }



    connect(nview, &navg::NavGMapView::mouseDoubleClickedOnScene, this,
            &DvlSwitch::handleSceneMousePress );
}


void DvlSwitch::connectPlugin(const QObject *plugin,
                              const QJsonObject &metadata) {
    if (!plugin) {
        qCCritical(plugin_dvl_switch, "plugin not created, returning");
        return;
    }

    const auto name =
            metadata.value("MetaData").toObject().value("name").toString();
    if (name.isEmpty()) {
        return;
    }

    if (name == "navest") {
        if (!connect(plugin, SIGNAL(remoteAddressChanged(QHostAddress)), this,
                     SLOT(setNavestRemoteAddress(QHostAddress)))) {
            qCCritical(plugin_dvl_switch, "Unable to connect to signal: "
                                          "Navest::remoteAddressChanged("
                                          "QHostAddress)");
            Q_ASSERT(false);
        } else {
            qCDebug(plugin_dvl_switch, "connected to navest");
        }

        connect(plugin, SIGNAL(messageReceived(NavestMessageType, QVariant)), this,
                SLOT(updateFromNavestMessage(NavestMessageType, QVariant)));

        connect(this, SIGNAL(sendNavestMessage(const QByteArray)), plugin,
                SLOT(sendMessage(const QByteArray)));

        connect(ui->send_doppler_button, &QPushButton::clicked, this,
                &DvlSwitch::sendDopplerToUse);
        connect(ui->send_offset_button, &QPushButton::clicked, this,
                &DvlSwitch::sendDopplerOffset);
        connect(ui->rdi_radio, &QRadioButton::clicked, this,
                &DvlSwitch::handleDopplerChoice);
        connect(ui->syrinx_radio, &QRadioButton::clicked, this,
                &DvlSwitch::handleDopplerChoice);


        connect (ui->reset_button, &QPushButton::clicked, this, &DvlSwitch::handleResetButton);
    }
}

void DvlSwitch::loadSettings(QSettings *) {}

void DvlSwitch::saveSettings(QSettings &settings)
{
  settings.setValue("persist", 1);
}

QList<QAction *> DvlSwitch::pluginMenuActions() { return {}; }


void DvlSwitch::sendDopplerToUse() {
    // pass in doppler choice 0 or 1.
    // 0 for RDI, 1 for Syrinx

    char buffer[256];
    int len = snprintf(buffer, 256, "NAVCMD CHOOSEDVL %d", this->doppler_choice);
    if (len)
        ;
    // may have to send send to navest

    qCDebug(plugin_dvl_switch) << "Sending new doppler choice of:  " << buffer;

    emit sendNavestMessage(buffer);
}

void DvlSwitch::sendDvlReset(QPointF fix){
    char buffer[256];
    int len = sprintf(buffer, "NAVCMD RSTDVL GEOMAN %.8f %.8f", fix.x(), fix.y());
    if (len);

    qCDebug(plugin_dvl_switch) << "Sending DVL reset command:  " << buffer;

    emit sendNavestMessage(buffer);
}

void DvlSwitch::sendDopplerOffset() {
    double lower_range = -359.99;
    double upper_range = 359.99;
    char buffer[256];
    int len = 0;
    int doppler = this->doppler_choice;

    double offset = ui->offset_entry->text().toDouble();

    // some extra bound checking since QDoubleValidator has it's flaws...
    if (!(offset >= lower_range && offset <= upper_range))
        return;

    if (doppler == 0) {
        len = snprintf(buffer, 256, "NAVCMD RDVLOFF %.2f", offset);
    } else if (doppler == 1) {
        len = snprintf(buffer, 256, "NAVCMD SDVLOFF %.2f", offset);
    }
    else return;

    if (len) {
        qCDebug(plugin_dvl_switch) << "Sending doppler offset: " << buffer;
        emit sendNavestMessage(buffer);
    }

    // send to navest plugin to be transmitted
}

void DvlSwitch::handleDopplerChoice(bool is_checked) {
    if (ui->rdi_radio == (QRadioButton *)sender()) {
        if (is_checked) {
            this->doppler_choice = 0;
        }
    } else if (ui->syrinx_radio == (QRadioButton *)sender()) {
        if (is_checked) {
            this->doppler_choice = 1;
        }
    }
}

void DvlSwitch::handleResetButton(){
    QPointF fix;
    if (ui->reset_combo->currentIndex() == 0)  {
        //use last usbl fix
        fix = ui->reset_combo->itemData(0).value<QPointF>();


    }
    else if (ui->reset_combo->currentIndex() == 1){
        //use cursor position as fix
        fix = ui->reset_combo->itemData(1).value<QPointF>();
    }
    else return;
    if (fix.isNull()) return;

    this->sendDvlReset(fix);
}

void DvlSwitch::updateFromNavestMessage(NavestMessageType type,
                                        QVariant message) {

    if (type == NavestMessageType::TDR) {
      auto timeElapsed = message.value<NavestMessageTdr>().time_since_reset;
      ui->TdrValueLbl->setText(QString::number(timeElapsed, 'f', 1));
    }
    if (type == NavestMessageType::DDD) {
      auto dddMsg = message.value<NavestMessageDdd>();
      ui->r1Lbl->setText(QString::number(dddMsg.beam_ranges[0], 'f', 2));
      ui->r2Lbl->setText(QString::number(dddMsg.beam_ranges[1], 'f', 2));
      ui->r3Lbl->setText(QString::number(dddMsg.beam_ranges[2], 'f', 2));
      ui->r4Lbl->setText(QString::number(dddMsg.beam_ranges[3], 'f', 2));

      ui->v1Lbl->setText(QString::number(dddMsg.beam_velocities[0], 'f', 2));
      ui->v2Lbl->setText(QString::number(dddMsg.beam_velocities[1], 'f', 2));
      ui->v3Lbl->setText(QString::number(dddMsg.beam_velocities[2], 'f', 2));
      ui->v4Lbl->setText(QString::number(dddMsg.beam_velocities[3], 'f', 2));

      ui->BeamsValueLbl->setText(QString::number(dddMsg.bt_beams, 'f', 2));
    }
    if (type == NavestMessageType::VFR) {
        QString fix_source = message.value<NavestMessageVfr>().fix_source;
        if (fix_source != "SOLN_USBL"){
            return;
        }
        int vehicle_id = message.value<NavestMessageVfr>().vehicle_number;
        if (vehicle_id != 0) {
            return;
            //jasons vehicle id is 0 usually
        }
        double lat = message.value<NavestMessageVfr>().lat;
        double lon = message.value<NavestMessageVfr>().lon;

        //index 1 meaning setting by last fix location
        ui->reset_combo->setItemData(1,QPointF(lat,lon));
        if (ui->reset_combo->currentIndex() == 1){

            //we are setting last usbl fix in the qcombo item itself. Since fix created here, we
            //dont directly need to pull that data back and can just use our lat lon vars
            ui->reset_latlon_label->setText(QString::number(lat) + " " + QString::number(lon));
        }
    }
}

void DvlSwitch::handleResetComboChanged(int index){
    ui->reset_latlon_label->setText("Lat Lon");
    //reset the Coordinates stored so we don't end up with previous data
    ui->reset_combo->setItemData(0,QPointF{});
    ui->reset_combo->setItemData(1,QPointF{});

}

void DvlSwitch::setNavestRemoteAddress(QHostAddress address) {
    m_navest_remote_address = address;
}



// slot for receiving mapview key press
void DvlSwitch::handleSceneMousePress(QPointF cursor) {

    //transform coords appropriately before sending

    if (ui->reset_combo->currentIndex() == 0) {
        auto map_scene = m_view->mapScene();

        if (!map_scene) {

            return;
        }
        double lon = cursor.x();
        double lat = cursor.y();
        auto proj = map_scene->projection();
        proj->transformToLonLat(1,&lon,&lat);

        //index 0 meaning cursor position option
        ui->reset_combo->setItemData(0,QPointF(lat,lon));
        ui->reset_latlon_label->setText(QString::number(lat) + " " + QString::number(lon));
    }

}

} // jason
} // plugins
} // navg
