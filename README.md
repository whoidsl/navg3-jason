# NavG3 Jason #

The Jason-specific plugins for NavG3 that do not depend on ROS/catkin. 
More generic plugins are stored in the navg3-git repository.

### Setup and build instructions ###

* Install NavG3 according to its instructions 
(https://bitbucket.org/whoidsl/navg3-git)
* Create a build folder, then build and install the project. 
* $ mkdir build
* $ cd build
* $ cmake ../
* $ make
* $ sudo make install
* Confirm that plugins exist in you /usr/bin with names like 
libnavg_sentry_acomms.so
* Run navg. Confirm that it finds the jason-specific plugins 
by loading them via the plugins bar.

### Contribution guidelines ###

* Use separate branches/pull requests for major features.
* Use doxygen format when commenting so that 
* Before checking in code, run the clang-format executable 
to align code to standard format.
* If a plugin becomes especialy useful and seems to belong 
in navg3-git, submit it as a pull request there. Once 
accepted, remove it from this repo.

### Creating another vehicle plugin repo

* Make sure that you namespace with similar conventions in 
order for NavG3 to find the new plugins.
